#include <mpi.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <numeric>
#include <ctime>

using namespace std;

// Функція для обчислення загальної вартості шляху
int calculateCost(const vector<vector<int>>& graph, const vector<int>& path) {
    int cost = 0;
    for (int i = 0; i < path.size() - 1; i++) {
        cost += graph[path[i]][path[i + 1]];
    }
    cost += graph[path.back()][path.front()];
    return cost;
}

// Головна функція
int main(int argc, char* argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Припустимо, що ми маємо 10 міст і випадкова вартість переходу між ними
    int n = 11;  // Змініть це число для експериментів з різною кількістю міст
    vector<vector<int>> graph(n, vector<int>(n));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            graph[i][j] = (i != j) ? (rand() % 20 + 10) : 0; // Рандомні вартості від 10 до 30, крім діагоналі
        }
    }

    vector<int> path(n);
    iota(path.begin(), path.end(), 0);

    int global_min = INT_MAX;
    double start_time = MPI_Wtime();  // Початок вимірювання часу

    do {
        // Поділ роботи між процесами
        if (rank == distance(path.begin(), find(path.begin(), path.end(), 0)) % size) {
            int cost = calculateCost(graph, path);
            global_min = min(global_min, cost);
        }
    } while (next_permutation(path.begin(), path.end()));

    double end_time = MPI_Wtime(); // Закінчення вимірювання часу

    // Збір результатів з усіх процесів
    int final_min;
    MPI_Reduce(&global_min, &final_min, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        cout << "Minimum cost: " << final_min << endl;
        cout << "Elapsed time: " << end_time - start_time << " seconds." << endl;
    }

    MPI_Finalize();
    return 0;
}


#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <numeric>
#include <omp.h>

using namespace std;


int factorial(int n) {
    int result = 1;
    for (int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}


int calculateCost(const vector<vector<int>>& graph, const vector<int>& path) {
    int cost = 0;
    for (int i = 0; i < path.size() - 1; i++) {
        cost += graph[path[i]][path[i + 1]];
    }
    cost += graph[path.back()][path.front()];
    return cost;
}

int main() {

    int n = 11;
    vector<vector<int>> graph(n, vector<int>(n));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            graph[i][j] = (i != j) ? (rand() % 20 + 10) : 0;  
        }
    }

    vector<int> path(n);
    iota(path.begin(), path.end(), 0);

    int global_min = INT_MAX;
    double start_time = omp_get_wtime();  
    #pragma omp parallel
    {
        int local_min = INT_MAX;
        vector<int> local_path(path);

        #pragma omp for nowait
        for (int i = 0; i < factorial(n); ++i) {
            next_permutation(local_path.begin(), local_path.end());
            int cost = calculateCost(graph, local_path);
            local_min = min(local_min, cost);
        }

        #pragma omp critical
        {
            global_min = min(global_min, local_min);
        }
    }

    double end_time = omp_get_wtime();  

    cout << "Minimum cost: " << global_min << endl;
    cout << "Elapsed time: " << end_time - start_time << " seconds." << endl;

    return 0;
}
